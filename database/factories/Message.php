<?php

use Faker\Generator as Faker;
use App\Messages;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Messages::class, function (Faker $faker) {
  $receiverEmails = [];
  for($i = 0; $i < 5; $i++) {
    $receiverEmails[] = $faker->email;
  }
    return [
        'subject' => $faker->text,
        'senderEmail' => $faker->email,
        'receiverEmail' => json_encode($receiverEmails),
        'replyToEmail' => $faker->email,
        'body' => $faker->text,
        'sendAt' => $faker->dateTime,
        'status' => 'new',
        'userId' => rand( 1, 10000)
    ];
});
