<?php
/**
 *
 * @author     AvaWu<avawuyf@hotmail.com>
 */

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('templates', function (Blueprint $table) {
            $table->increments('id');
            $table->text('header');
            $table->string('title')->nullable();
            $table->string('pageTop')->nullable();
            $table->string('pageBody');
            $table->string('pageBottom')->nullable();
            $table->text('footer');
            $table->text('sectionBannerHtml')->nullable();
            $table->text('sectionCellHtml')->nullable();
            $table->text('RrpPriceHtml')->nullable();
            $table->text('MemberPriceHtml')->nullable();
            $table->text('SpecialPriceHtml')->nullable();
            $table->text('discriptionHtml')->nullable();
            $table->text('skuHtml')->nullable();
            $table->text('proNameHtml')->nullable();
            $table->text('proImageHtml')->nullable();
            $table->string('headerLink')->nullable();
            $table->string('footerLink')->nullable();
            $table->text('singleDiscription')->nullable();
            $table->string('proItemTop')->nullable();
            $table->string('proItemBottom')->nullable();
            $table->boolean('allowNoProduct')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('templates');
    }
}
