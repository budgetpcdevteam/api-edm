<?php
/**
 * This is the CreateMessagesTable
 *
 * @author     AvaWu<avawuyf@hotmail.com>
 */
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('subject');
            $table->string('senderEmail');
            $table->string('receiverEmail');
            $table->string('replyToEmail')->nullable();
            $table->dateTime('sendAt');
            $table->text('body');
            $table->string('status');
            $table->timestamps();
            // $table->unsignedBigInteger('userId');
            $table->integer('userId')->unsigned();
            // $table->integer('attachmentId')->unsigned();
            // $table->foreign('userId')->references('id')->on('users');
            // $table->unsignedBigInteger('attachmentId');
        });
        Schema::table('messages', function($table) {
          $table->foreign('userId')->references('id')->on('users');
          // $table->foreign('attachmentId')->references('id')->on('attachments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages');
    }
}
