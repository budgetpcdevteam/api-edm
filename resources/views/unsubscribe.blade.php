<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Unsubscribe</title>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: Helvetica, Arial, sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            /* .full-height {
                height: 100vh;
            } */

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }
            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
            .container{
              max-width: 960px;
              background: #fafafa;
              border-radius: 2px;
              box-sizing: border-box;
              box-shadow: 0 0 5px 1px #ddd;
              margin: 30px auto 40px;
              padding: 40px;
              border: 1px solid #cacaca;
              display: flex;
              flex-wrap: wrap;
              flex-direction: column;
              justify-content: space-between;
              align-items: center;
            }
            .unsubscribe{
              min-width: 300px;
              text-align: center;
              width: 40% !important;
            }
            .unsubscribe-btn
            {
              width: 40%;
              background: #d30014;
              color: #ffffff;
              font-weight: 600;
              font-size: 14px;
              padding: 0.5rem;
            }
            .unsubscribe-text
            {
              color: #000000;
              margin: 2rem;
              font-weight: 500;
            }
            .modal-body
            {
              color: #000000;
              font-weight: 600;
              text-align: center;
              font-family: Helvetica, Arial, sans-serif;
            }
            .modal-header
            {
              border-bottom: none;
            }
            .modal-footer
            {
              border-top:none;
            }
        </style>
    </head>
    <body>
      <div>
        <div class="flex-center position-ref full-height">
                <div class="container unsubscribe">
                  <div class="row">
                    <div>
                      <img id = "unsbuscribePageLogo" style="max-width:420px;height:auto;display:none;" >
                      <h4 class="unsubscribe-text">Are you sure to unsubscribe from our newsletter?</h4>
                    </div>
                  </div>
                  <button class="unsubscribe-btn btn btn-lg" id="unsubscribe">Unsubscribe</button>
                </div>
              </div>
                <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">

                            </div>
                            <div class="modal-footer">
                                <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                            </div>
                        </div>
                    </div>
                  </div>
            </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script type="text/javascript">
        $(document).ready(function(){
          var pageurl = window.location.href;
          var pageUrlObj = new URL(pageurl);
          var logoUrl = pageUrlObj.searchParams.get("logoUrl");
          var pageLogoUrl = atob(logoUrl)
          var urlParaEmail = pageUrlObj.searchParams.get("token");
          var urlParaCompany = pageUrlObj.searchParams.get("com");
          if(logoUrl != null){
              $('#unsbuscribePageLogo').attr("src", pageLogoUrl);
              $('#unsbuscribePageLogo').show();
          }

        $('#unsubscribe').click(function(e){
            e.preventDefault();
              $.ajax({
                type: "post",
                url: "api/unsubscribe/confirm",
                dataType: "json",
                data:{
                  "emailAddress" : urlParaEmail,
                  "companyUserId" : urlParaCompany
                },
                success:function(data){
                   $("#myModal").modal("show");
                   $(".modal-body").text(data.Message);
                }
              });
            });
          });
        </script>
    </body>
</html>
