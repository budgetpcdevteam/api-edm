<?php
/**
 *
 * @author     AvaWu<avawuyf@hotmail.com>
 */

use Illuminate\Http\Request;
use App\Article;
use App\Company;
use App\Messages;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


// Route::middleware('auth:api')->get('articles', 'ArticleController@index');
// Route for articles:

// Route for Users
// register information example:{"name": "AvaWu", "email": "test@kppc.com.au", "password": "test123", "password_confirmation": "test123"}
Route::post('register', 'Auth\RegisterController@register');

Route::post('login', 'Auth\LoginController@login');

// Route for Messages

Route::get('messages/get', 'MessageController@messagesIndex');
Route::get('messages', 'MessageController@showMessages');
Route::post('messages', 'MessageController@storeMessages');
Route::put('messages/{messages}', 'MessageController@updateMessages');
Route::delete('messages/{messages}', 'MessageController@deleteMessages');

// Route for Company
Route::get('company', 'CompanyController@index');
Route::get('company', 'CompanyController@show');
Route::post('company', 'CompanyController@store');
Route::put('company/{company}', 'CompanyController@update');
Route::delete('company/{company}', 'CompanyController@delete');
Route::get('company/getname', 'CompanyController@testName');

// Route for Template
// Route::get('template/all', 'TemplateController@templateIndex');
// Route::get('template', 'TemplateController@showTemplate');
Route::get('template/ids', 'TemplateController@getTemplateId');
// Route::post('template', 'TemplateController@storeTemplate');
// Route::put('template/{template}', 'TemplateController@updateTemplate');
// Route::delete('template/{template}', 'TemplateController@deleteTemplate');
// Route::post('template/senddata', 'TemplateController@receivedData');

// should use token to post data
Route::middleware('auth:api')->post('template/senddata', 'TemplateController@receivedData');
Route::middleware('auth:api')->post('template/precheck', 'TemplateController@precheckBaseData');
Route::middleware('auth:api')->get('template/all', 'TemplateController@templateIndex');
Route::middleware('auth:api')->post('template', 'TemplateController@storeTemplate');
// Route::post('logout', 'Auth\LoginController@logout');
Route::post('/unsubscribe/confirm', 'UnsubscribeController@storeUnsubscribeEmailAd');
Route::put('template/updateemailconfig', 'UserController@updateMailSenderConfigs');
// Route::group(['middleware' => 'auth:api'], function() {
//     Route::get('articles', 'ArticleController@index');
//     Route::get('articles/{article}', 'ArticleController@show');
//     Route::post('articles', 'ArticleController@store');
//     Route::put('articles/{article}', 'ArticleController@update');
//     Route::delete('articles/{article}', 'ArticleController@delete');
// });
