<?php
/**
 * This is the TemplateController
 *
 * @author     AvaWu<avawuyf@hotmail.com>
 */
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Template;
use App\Messages;
use Exception;
use Illuminate\Validation\Rule;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\UnsubscribeList;
use PHPUnit\Framework\Constraint\Count;

class TemplateController extends Controller
{
    //
    public function templateIndex()
    {
        return Template::all();
    }

    public function showTemplate(Template $template)
    {
        return $template;
    }

    public function getTemplateId(Template $template)
    {
        try {
            $idsArr = Template::select(['id', 'tplName'])
               ->orderBy('created_at', 'desc')
               ->get()
               ->toArray();
            if (count($idsArr) > 0) {
                return response()->json(['Success' => true,'Message' => $idsArr], 200);
            } else {
                return response()->json(['Success' => false,'Message' => "No email templates"], 500);
            }
        } catch (\Exception $exception) {
            $errorInfo = $exception->getMessage();
            return response()->json(['Message' => $errorInfo], 500);
        }
    }

    public function storeTemplate(Request $request)
    {
        $template = Template::create($request->all());

        return response()->json($template, 201);
    }

    public function updateTemplate(Request $request, Template $template)
    {
        $template->update($request->all());
        return response()->json($template, 200);
    }

    public function deleteTemplate(Template $template)
    {
        $template->delete();

        return response()->json(null, 204);
    }

    public function receivedData(Request $request)
    {
        $templateItems = array();
        // receive data from api request
        $products = $request->input('products');
        $recipients = $request->input('recipientList');
        $subject = $request->input('emailSubject');
        $senderEmail = $request->input('fromEmail');
        $senderEmailText = (null !== $request->input('fromEmailText') ? '|' . $request->input('fromEmailText') : '');
        $replyToEmail = $request->input('replyToEmail');
        $reportErrorEmail = $request->input('reportErrorEmail');
        $status = "new";
        $userId = $request->input('userId');
        $sendAtDate = $request->input('sendAtDate');
        $templateId = $request->input('templateId');
        $templateHeaderImage = $request->input('headerImageUrl');
        $templateFooterImage = $request->input('footerImageUrl');
        $companyDomain = $request->input('companyDomain');
        $companyName = $request->input('companyName');
        $companyAdd = $request->input('companyAddress');
        $unSubscribeLogoUrl = $request->input('unSubscribeLogoUrl');
        $emailTemplate='';
        $tplHeader = '';
        $tplTitle = '';
        $tplSectionBanner = '';
        $tplPageTop = '';
        $tplPageBody = '';
        $tplFooter = '';
        $errorInfo = '';
        $companyUserId = \Auth::user()->id;

        if (!filter_var($senderEmail, FILTER_VALIDATE_EMAIL)) {
            return response()->json(['Success' => false,'Message' => "'fromEmail' is an invalid email address, please check."], 500);
        }
        if (!filter_var($replyToEmail, FILTER_VALIDATE_EMAIL)) {
            return response()->json(['Success' => false,'Message' => "'replyToEmail' is an invalid email address, please check."], 500);
        }
        // obtain template usitng template id from api
        // get tpl obj
        try {
            $emailTemplate = Template::where('id', '=', $templateId)->firstOrFail();
            $allowNoProduct = $emailTemplate->allowNoProduct;
            $lineProductNo = $emailTemplate->lineProductNo;
            $proItemTop = $emailTemplate->proItemTop;
            $proItemBottom = $emailTemplate->proItemBottom;
            // extract header, title, page top, page bod, page bottom, footer from tpl obj
            $emialTopWra = '<body style="background-color: #D3D3D3;font-family: Arial, Helvetica, sans-serif;"><div style="background-color: #D3D3D3;"><table style="width: 100%; height: 100%;" cellspacing="0" cellpadding="0" border="0">
                        <tbody><tr><td valign="top" align="center"><table width="800" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td valign="top" bgcolor="#FFFFFF" align="left"><table width="800" cellspacing="0" cellpadding="0" border="0">
                        <tbody><tr><td><table width="100%" height="100%" cellspacing="0" cellpadding="0" border="0"><tbody>';
            $emailBottomWra= '</tbody></table></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></div></body>';
            $headerTopWrapper ='<tr><td><table style="border-collapse: collapse;" class="devicewidth" width="800" cellspacing="0" cellpadding="0" border="0"><tbody><tr>
                            <td style="font-size: 0; line-height: 100%;padding:10px;" align="center"><a target="_blank" style="color: #000000; text-decoration: underline;" href="' . $companyDomain . '">
                            <img style="max-width: 100% !important; width: 100% !important; height: auto; display:block !important;font-size: 12px;" alt="" title="" src="';
            $headerBottomWrapper = '" border="0"></a></td></tr></tbody></table></td></tr>';
            $footerTopWrapper = '<tr><td><table style="border-collapse: collapse;margin-top:15px;" width="800" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td style="font-size: 0; line-height: 100%;padding-left:10px;padding-right:10px;padding-bottom:10px;"><a target="_blank" style="color: #000000; text-decoration: underline;" href="' . $companyDomain . '">
                            <img style="max-width: 100% !important; width: 100% !important; height: auto; display:block !important;font-size: 12px;" alt="" title="" src="';
            if ($companyName != 'Electronic Security Wholesalers Pty Ltd'){
                $footerBottomWrapper=
                    ' " border="0"></a></td></tr>
                        <tr><td style="text-align:center">
                            <p><span>Sent by <a target="_blank" style="color: #000000; text-decoration: underline;" href="' . $companyDomain . '">' . $companyName . ' </a> - ' . $companyAdd . '  </span><span><a href="{TPL_UNSUB_LINK}" target="_blank" style="color: #000000; text-decoration: underline;"> Unsubscribe</a></span></p>
                            <p style="margin-top:0;margin-bottom:0"><span style="font-weight:100">BPC Technology reserves the rights to change the promotional items & prices without prior notice.</span></p>
                            <p style="margin-top:0;margin-bottom:0"><span style="font-weight:100">If you have any questions please contact us.</span></p>
                            <p style="margin-top:0;margin-bottom:0"><span style="font-weight:100">Copyright © 2024 '.$companyName.'.  All rights reserved.</span></p>
                        </td></tr>
                    </tbody></table></td></tr>';
            }else{
            	$footerBottomWrapper=' " border="0"></a></td></tr><tr><td style="text-align:center"><p><span>Sent by <a target="_blank" style="color: #000000; text-decoration: underline;" href="' . $companyDomain . '">' . $companyName . ' </a> ' . $companyAdd . '  </span></p></td></tr></tbody></table></td></tr>';
            }
            $tplHeader = $headerTopWrapper . $templateHeaderImage . $headerBottomWrapper;

            $tplPageBody = $emailTemplate->pageBody;
            $tplFooter = $footerTopWrapper . $templateFooterImage .$footerBottomWrapper;

            $productsHtml = '';

            foreach ($products as $product) {
                $bannerHtml = $emailTemplate->sectionBannerHtml;
                // obtain one product
                // extract product info from product obj
                $categoryBanner = $product["bannerUrl"];
                $categoryLink = $product["bannerLink"];
                $productItems = $product["productItems"];
                $productItemsHtml = '';
                $productLineItemsHtml = '';
                $productLineHtml = '';
                $bannerHtml = str_replace(array('{TPL_BANNER_LINK}','{TPL_BANNER_IMAGE_LINK}'), array($categoryLink, $categoryBanner), $bannerHtml);
                $productItemTal = 0;

                if (count($productItems) > 0 && (count($productItems)%$lineProductNo !== 0)) {
                  	if(trim($reportErrorEmail) != ''){
                    	Messages::create([
                              "subject" => 'Sending Newsletter Failed!',
                    		  "senderEmail" => $senderEmail . $senderEmailText,
                              "receiverEmail" => json_encode($reportErrorEmail),
                               "replyToEmail" => $replyToEmail,
                               "status" => $status,
                               // "userId" => $userId,
                               "userId" => $companyUserId,
                               "sendAt" => $sendAtDate,
                               "body" => "Product items should be consist with template item number " . $lineProductNo
                   		]);
                  	}
                    return response()->json(['Success' => false, 'Message' => "Product items should be consist with template item number " . $lineProductNo ], 500);
                }

                if (count($productItems) > 0) {
                    $productSubLineHtml = '';
                    foreach ($productItems as $productItem) {
                        $sku = "";
                        $proName = "";
                        $brand = '';
                        $proSpecialPrice = "";
                        $proMemberPrice = "";
                        $proRrPrice = "";
                        $discriptionArrs = array();
                        $proDetailLink =  "";
                        $proImageUrl = "";

                        if ($companyName != 'Electronic Security Wholesalers Pty Ltd'){
	                        $skuHtml = $emailTemplate->skuHtml;
	                        $proNameHtml = $emailTemplate->proNameHtml;
                        }else{
                        	$skuHtml = "<p style='font-style: italic; font-size: 10pt; text-align: center;font-family:Arial, Helvetica, sans-serif;margin:0;'>{TPL_SKU}</p>";
                        	$proNameHtml = "<p style='margin:0; padding-left: 20px; padding-right: 6px; text-align: center;font-family:Arial, Helvetica, sans-serif;'><span style='color: #000; text-decoration: none; font-size: 11pt;'>{TPL_PRO_NAME}</span></p>";
                        }
                        $proImageHtml = $emailTemplate->proImageHtml;
                        $RrpPriceHtml = $emailTemplate->RrpPriceHtml;
                        $memberPriceHtml = $emailTemplate->MemberPriceHtml;
                        $specialPriceHtml = $emailTemplate->SpecialPriceHtml;
                        $discriptionHtml = $emailTemplate->discriptionHtml;
                        $singleDiscriptionHtml = $emailTemplate->singleDiscription;

                        if ($productItem['sku'] != "") {
                            $sku = $productItem['sku'];
                        }
                        if ($productItem['name'] != "") {
                            $proName = $productItem['name'];
                        }
                        if (isset($productItem['brand']) && $productItem['brand'] != ''){
                        	$brand = $productItem['brand'];
                        }
                        if ($productItem['specialPrice'] != "") {
                            $proSpecialPrice = round($productItem['specialPrice'], 2);
                        }
                        if ($productItem['memberPrice'] != "") {
                            $proMemberPrice = round($productItem['memberPrice'], 2);
                        }
                        if ($productItem["rrPrice"] != "") {
                             $proRrPrice = round($productItem["rrPrice"], 2);
                        }
                        if (is_array($productItem['discriptionArr']) && count($productItem['discriptionArr']) > 0) {
                            $discriptionArrs = $productItem['discriptionArr'];
                        }
                        if ($productItem["detailLink"] != "") {
                            $proDetailLink =  $productItem["detailLink"];
                        }
                        if ($productItem["imageUrl"] != "") {
                            $proImaUrlInput = $productItem["imageUrl"];
                            if ((strpos($proImaUrlInput, 'http://') !== false) || (strpos($proImaUrlInput, 'https://') !== false)) {
                                $proImageUrl = $proImaUrlInput;
                            } else {
                                $proImageUrl = 'http://' . $proImaUrlInput;
                            }
                        }
                        $fullDiscription = $hreflink = $TPL_aTag_extraStyle = $TPL_aTag_extraHtmlAttr = '';
                        $skuHtml = str_replace('{TPL_SKU}', $sku, $skuHtml);
                        $proNameHtml = str_replace('{TPL_PRO_NAME}', $proName, $proNameHtml);
                        if($proDetailLink == ''){
                            $TPL_aTag_extraStyle = $TPL_aTag_extraStyle . 'cursor:default;';
                            $TPL_aTag_extraHtmlAttr = $TPL_aTag_extraHtmlAttr . "onclick='return false;'";
                        }else{
                            $hreflink = "href = '".$proDetailLink."'";
                        }
                        $proImageHtml = str_replace(array('{TPL_PRO_LINK}','{TPL_PRO_IMAGE_LINK}', '{TPL_PRO_A_EXTRA_STYLE}', '{TPL_PRO_A_EXTRA_HTML_ATTR}'), array($hreflink, $proImageUrl, $TPL_aTag_extraStyle, $TPL_aTag_extraHtmlAttr), $proImageHtml);

                        foreach ($discriptionArrs as $signleDisctiption) {
                            $singleDiscriptionHtml = str_replace('{TPL_SINGLE_DISCRIPTION}', $signleDisctiption, $singleDiscriptionHtml);
                            $fullDiscription .= $singleDiscriptionHtml;
                            $singleDiscriptionHtml = $emailTemplate->singleDiscription;
                        }
                        if ($fullDiscription != ''){
                        	$discriptionHtml = str_replace('{TPL_PRO_DISCRIPTION}', $fullDiscription, $discriptionHtml);
                        }else{
                        	$discriptionHtml = '';
                        }
                        $proPrice = '';
                        $specialPrice ='';
                        $productCellHtml = '';
                        $proInfo = $proNameHtml.$discriptionHtml;
                        if ($brand != ''){
                        	$skuHtml = '<p style="font-size: 10pt; text-align: center; font-family:Arial, Helvetica, sans-serif;font-weight:600;">' . $brand . '</p>' . $skuHtml;
                        }
                        setlocale(LC_MONETARY, 'en_AU.UTF-8');
                        if ($proMemberPrice != '') {
                        	$proRrPrice  = ($companyName == 'Electronic Security Wholesalers Pty Ltd' ? '' : money_format('%.2n', $proRrPrice));
                        	$proMemberPrice  = money_format('%.2n', $proMemberPrice);
                            if ($companyName != 'Electronic Security Wholesalers Pty Ltd'){
                            	$proMemberPrice = $proMemberPrice . ' inc';
                            }
                            $memberPriceHtml = str_replace(array('{TPL_RRP_PRICE}','{TPL_MEMBER_PRICE}'), array($proRrPrice, $proMemberPrice), $memberPriceHtml);
                            $productCellHtml = $proItemTop . '<tr valign="top"><td>' . $proImageHtml . $skuHtml . $memberPriceHtml . '</td></tr><tr><td>' . $proInfo . "</td></tr>". $proItemBottom;
                        } elseif ($proSpecialPrice != '') {
                        	$proSpecialPrice  = money_format('%.2n', $proSpecialPrice);
                            if ($companyName != 'Electronic Security Wholesalers Pty Ltd'){
                            	$proRrPrice  = money_format('%.2n', $proRrPrice);
                            	$proSpecialPrice = $proSpecialPrice . ' inc';
                            	$specialPriceHtml = str_replace(array('{TPL_RRP_PRICE}','{TPL_SPECIAL_PRICE}'), array($proRrPrice, $proSpecialPrice), $specialPriceHtml);
                            	$productCellHtml = $proItemTop . '<tr valign="top"><td>' . $proImageHtml . $skuHtml . $specialPriceHtml . '</td></tr><tr><td>' . $proInfo . "</td></tr>" . $proItemBottom;
                            }else{
                            	$specialPriceHtml = "<p style='margin:0; font-family:Arial, Helvetica, sans-serif;align-items:center;justify-content:center;height: 30px; text-align: center;'>
									<strong style='font-size: 14pt; color: #ff0000;'>" . $proSpecialPrice. "<br></strong></p>";
                            	$productCellHtml = $proItemTop . '<tr valign="top"><td style="height:50%">' . $proImageHtml . $skuHtml . '</td></tr><tr><td>' . $proInfo . '</td></tr><tr valign="bottom"><td>' . $specialPriceHtml . '</td></tr>' . $proItemBottom;
                            }
                        } else {
                        	$proRrPrice  = money_format('%.2n', $proRrPrice);
                            if ($companyName != 'Electronic Security Wholesalers Pty Ltd'){
                            	$proRrPrice = $proRrPrice . ' inc';
                            }
                            $RrpPriceHtml = str_replace('{TPL_RRP_PRICE}', $proRrPrice, $RrpPriceHtml);
                            $productCellHtml = $proItemTop . '<tr valign="top"><td>' . $proImageHtml . $skuHtml . $RrpPriceHtml . '</td></tr><tr><td>' . $proInfo . "</td></tr>" . $proItemBottom;
                        }
                        $productItemTal++;
                        $tdWidth = '100%';
                        $tdWidthTag = '800';
                        if ($lineProductNo > 0) {
                            $tdWidth = round(100/$lineProductNo, 3) . "%";
                            $tdWidthTag = round(800/$lineProductNo, 0);
                        }
                        $productLineItemsHtml .= '<td style = "height: 100%;width:' . $tdWidth .'"' .'width="'. $tdWidthTag .'" >'. $productCellHtml . "</td>";
                        if ($productItemTal%$lineProductNo === 0) {
                            $productSubLineHtml = '<tr><td width="100%"><table style="border-collapse: collapse;" width="800" cellspacing="0" cellpadding="0" border="0"><tbody><tr style="height: auto;" valign="top">' . $productLineItemsHtml . '</tr></tbody></table></td></tr>';
                            $productItemsHtml .= $productSubLineHtml;
                            $productLineItemsHtml = '';
                            $productSubLineHtml = '';
                        }
                    }
                }
                $productLineHtml = '<tr valign="top">' . $bannerHtml . '</tr>' . $productItemsHtml;
                $productsHtml .= $productLineHtml;
            }

            // replace/insert page body tpl with tartet products html
            // string replace in PHP for tpl target like TPL_BODY
            // pageBody column in database could build like this: '<p>TPL_BODY</p>'
            $tplPageBody = str_replace('{TPL_BODY}', $productsHtml, $tplPageBody);
            $head = '<head><meta name="robots" content="noindex, nofollow"><META HTTP-EQUIV="expires" VALUE="Sat, 1 Jan 2000 11:00:00 GMT"><META HTTP-EQUIV="pragma" CONTENT="no-cache"><title></title><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	               <meta name="viewport" content="width=device-width, initial-scale=1.0"><style type="text/css">body, table, tr, td, div, textarea, input{color:#000000;font-family: Helvetica, Arial, sans-serif;font-size:12px;}</style><style type="text/css"> table,table td,table th{border-collapse:collapse;}</style></head>';
              // for loop all the email address from api post
             // create message model
            $numberOfMessages = 0;
            $unsubscribeNo = 0;


            if (($recipientTotal = count($recipients)) > 0) {
                $recipientsArray = array();
                $unsubscribeEmails = UnsubscribeList::select('emailAddress')->where('companyUserId', '=', $companyUserId)->get()->pluck('emailAddress')->toArray();
                foreach ($recipients as $recipient) {
                    if(!in_array($recipient, (array) $unsubscribeEmails)){
                        if(filter_var($recipient, FILTER_VALIDATE_EMAIL))
                        {
                          $recipientsArray[] = $recipient;
                          $numberOfMessages++;
                        }
                    }else{
                        $unsubscribeNo++;
                    }
                }
                if(count((array) $recipientsArray) > 0){
                    $fullTemplateHtml = '<!DOCTYPE html><html>'. $head . $emialTopWra .$tplHeader . $tplPageBody . $tplFooter . $emailBottomWra .'</html>';
                    if ($companyName != 'Electronic Security Wholesalers Pty Ltd'){
	                    $unsubLink = $this->unsubscribeUrl($unSubscribeLogoUrl, $companyUserId);
	                    $fullTemplateHtml = str_replace('{TPL_UNSUB_LINK}', $unsubLink, $fullTemplateHtml);
                    }

                    $recipientsArrays = array_chunk($recipientsArray, 1000);
                    foreach($recipientsArrays as $recipientsArray){
                        Messages::create([
                        		"subject" => $subject,
                        		"senderEmail" => $senderEmail . $senderEmailText,
                                "receiverEmail" => json_encode($recipientsArray),
                                "replyToEmail" => $replyToEmail,
                                "status" => $status,
                                // "userId" => $userId,
                                "userId" => $companyUserId,
                                "sendAt" => $sendAtDate,
                                "body" => $fullTemplateHtml
                       ]);
                    }

                }
                if ($numberOfMessages < $recipientTotal) {
                    return response()->json(['Success' => true, 'Message' => $numberOfMessages . " Messages are Created Successfully. " . $unsubscribeNo . " email address(es) are in UnsubscribeList. " .($recipientTotal - $numberOfMessages -$unsubscribeNo) . " email address(es) are invalid."], 200);
                } else {
                    return response()->json(['Success' => true,'Message' => $numberOfMessages . " Messages are Created Successfully."], 200);
                }
            } else {
              	// if(trim($reportErrorEmail) != ''){
                    if(count((array) $reportErrorEmail) > 0){
                	Messages::create([
                        "subject" => 'Sending Newsletter Failed!',
                		"senderEmail" => $senderEmail . $senderEmailText,
                        "receiverEmail" => json_encode($reportErrorEmail),
                        "replyToEmail" => $replyToEmail,
                        "status" => $status,
                        // "userId" => $userId,
                        "userId" => $companyUserId,
                        "sendAt" => $sendAtDate,
                        "body" => "API doesn't receieve any valid recipient!"
                    ]);
              	}
                return response()->json(['Success' => false,'Message' => $numberOfMessages . " message are Created. Please check your recipientList"], 500);
            }
        } catch (\Exception $exception) {
            $errorInfo = $exception->getMessage();
            if(trim($senderEmail) != '' && count((array) $reportErrorEmail) > 0 && trim($replyToEmail) != ''){
              Messages::create([
                        "subject" => 'Sending Newsletter Failed!',
              			"senderEmail" => $senderEmail . $senderEmailText,
                        "receiverEmail" => json_encode($reportErrorEmail),
                         "replyToEmail" => $replyToEmail,
                         "status" => $status,
                         // "userId" => $userId,
                         "userId" => $companyUserId,
                         "sendAt" => $sendAtDate,
                         "body" => $errorInfo
                        ]);
            }
            return response()->json(['Success' => false, 'Message' => $errorInfo], 500);
        }
    }

    public function unsubscribeUrl($unSubscribeLogoUrl, $companyUserId)
    {
        // following link need to be change to real link $unSubscribeLogoUrl, $companyUserId
        // $unsubscribeLink = "http://localhost:8000/unsubscribe";
        $unsubscribeLink = "https://edmapi.merp.com.au/unsubscribe";
        // $encryptEmail = base64_encode($emailAddress);
        $encryptlogo = base64_encode($unSubscribeLogoUrl);
        $encryptUserId = base64_encode($companyUserId);
        $personalUnsubLink =  $unsubscribeLink . "?token=" ."{ENCRYPT_RECIPIENT_EMAIL_ADDRESS}" . "&logoUrl=" . $encryptlogo . "&com=" . $encryptUserId;
        return $personalUnsubLink;
    }
    public function precheckBaseData(Request $request){
      $templateItems = array();
      $products = $request->input('products');
      $recipients = $request->input('recipientList');
      $senderEmail = $request->input('fromEmail');
      $replyToEmail = $request->input('replyToEmail');
      $companyUserId = \Auth::user()->id;
      $templateId = $request->input('templateId');
      if (!filter_var($senderEmail, FILTER_VALIDATE_EMAIL)) {
          return response()->json(['Success' => false,'Message' => "'fromEmail' is an invalid email address, please check."], 500);
      }
      if (!filter_var($replyToEmail, FILTER_VALIDATE_EMAIL)) {
          return response()->json(['Success' => false,'Message' => "'replyToEmail' is an invalid email address, please check."], 500);
      }
      // obtain template usitng template id from api
      // get tpl obj
      try {
          $emailTemplate = Template::where('id', '=', $templateId)->firstOrFail();
          $allowNoProduct = $emailTemplate->allowNoProduct;
          $lineProductNo = $emailTemplate->lineProductNo;
          $proItemTop = $emailTemplate->proItemTop;
          $proItemBottom = $emailTemplate->proItemBottom;
          $productsHtml = '';

          foreach ($products as $product) {
              $categoryBanner = $product["bannerUrl"];
              $categoryLink = $product["bannerLink"];
              $productItems = $product["productItems"];
              $productItemsHtml = '';
              $productLineItemsHtml = '';
              $productLineHtml = '';
              $productItemTal = 0;

              if (count($productItems) > 0 && (count($productItems)%$lineProductNo !== 0)) {
                  return response()->json(['Success' => false, 'Message' => "Product items should be consist with template item number " . $lineProductNo ], 500);
              }

          }

          $numberOfMessages = 0;
          $unsubscribeNo = 0;
          if (($recipientTotal = count($recipients)) > 0) {
              $unsubscribeEmails = UnsubscribeList::select('emailAddress')->where('companyUserId', '=', $companyUserId)->get()->pluck('emailAddress')->toArray();

              // return response()->json(['Success' => false,'Message' => json_encode($unsubscribeEmails), 500);
              foreach ($recipients as $recipient) {
                if(!in_array($recipient, $unsubscribeEmails)){
                    if(filter_var($recipient, FILTER_VALIDATE_EMAIL))
                    {
                      $numberOfMessages++;
                    }
                }else{
                    $unsubscribeNo++;
                }

                  // $unsubscribeCheck = UnsubscribeList::where('emailAddress', '=', $recipient)->where('companyUserId', '=', $companyUserId)->get();
                  // if (count($unsubscribeCheck) == 0 && filter_var($recipient, FILTER_VALIDATE_EMAIL)) {
                  //     $numberOfMessages++;
                  // } elseif (count($unsubscribeCheck) > 0) {
                  //     $unsubscribeNo++;
                  // }
              }
              if ($numberOfMessages < $recipientTotal) {
                  return response()->json(['Success' => true, 'Message' => $numberOfMessages . " Newsletters are expected to be sending. " . $unsubscribeNo . " email address(es) are in UnsubscribeList. " .($recipientTotal - $numberOfMessages -$unsubscribeNo) . " email address(es) are invalid."], 200);
              } else {
                  return response()->json(['Success' => true,'Message' => $numberOfMessages . " Newsletters are expected to be sending."], 200);
              }
          } else {
              return response()->json(['Success' => false,'Message' => $numberOfMessages . " message are Created. Please check your recipientList"], 500);
          }
      } catch (\Exception $exception) {
          $errorInfo = $exception->getMessage();
          return response()->json(['Message' => $errorInfo], 500);
      }
    }
}
