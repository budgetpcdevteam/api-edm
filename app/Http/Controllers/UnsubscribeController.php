<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UnsubscribeList;
use Exception;

class UnsubscribeController extends Controller
{
    public function storeUnsubscribeEmailAd(Request $request)
    {
        $emailAddress = $request->input('emailAddress');
        $companyUserId = $request->input('companyUserId');
        $decryptedEmail = '';
        $decryptedComUserId = '';
        try {
            $decryptedEmail = base64_decode($emailAddress);
            $decryptedComUserId = base64_decode($companyUserId);

            $unsubscribeCheck = UnsubscribeList::where('emailAddress', '=', $decryptedEmail)->where('companyUserId', '=', $decryptedComUserId)->get();
            if (count($unsubscribeCheck) > 0) {
                return response()->json(['Success' => true,'Message' => 'This email address already unsubscribed before. Thank You!'], 200);
            } else {
                if ($decryptedEmail != "" && $decryptedComUserId !="") {
                    $unsubscribeEmail = UnsubscribeList::create([
                      "emailAddress" => $decryptedEmail,
                      "companyUserId" => $decryptedComUserId
                     ]);
                    return response()->json(['Success' => true,'Message' => 'Unsubscribe Successfully. Thank You!'], 200);
                } else {
                    throw new Exception('No email address passed in!');
                }
            }
        } catch (\Exception $exception) {
            $errorInfo = $exception->getMessage();
            return response()->json(['Success' => false,'Message' => $errorInfo], 500);
        }
    }
}
