<?php
/**
 * This is the TemplateController
 *
 * @author     AvaWu<avawuyf@hotmail.com>
 */
namespace App\Http\Controllers;
// require_once '/path/to/vendor/autoload.php';

use Illuminate\Http\Request;
// use App\Template;
use App\User;
use Exception;
use Illuminate\Validation\Rule;
use Illuminate\Database\Eloquent\ModelNotFoundException;
// use App\UnsubscribeList;
use PHPUnit\Framework\Constraint\Count;
// use Egulias\EmailValidator\EmailValidator;
// use Egulias\EmailValidator\Validation\RFCValidation;

class UserController extends Controller
{
    public function updateMailSenderConfigs(Request $request){

        $userId = $request->input('userId');
        try{
            $user = User::where('id', '=', $userId)->firstOrFail();
            if(!$user instanceof User){
                throw new Exception('error user Id');
            }
            $host = $request->input('host');
            $port = $request->input('port');
            $ecryption = $request->input('host_encryption');
            $hostUsername = $request->input('host_username');
            $hostPassword = $request->input('host_password');
            if(isset($host) && strlen(trim($host))>0){
                $user->newsletter_sender_host = $host;
            }
            if(isset($port) && strlen(trim($port))>0){
                $user->newsletter_sender_port = $port;
            }
            if(isset($hostUsername) && strlen(trim($hostUsername))>0){
                $user->newsletter_host_username = $hostUsername;
            }
            if(isset($hostPassword) && strlen(trim($hostPassword))>0){
                $user->newsletter_host_password = $hostPassword;
            }
            if(isset($ecryption) && strlen(trim($ecryption))>0){
                $user->newsletter_sender_encryption = $ecryption;
            }
            $user->save();
            return response()->json($user);



            // $message->status = 'sending';
            // $message->save();
        }catch (\Exception $exception) {
            $errorInfo = $exception->getMessage();
            return response()->json(['Message' => $errorInfo], 500);
        }

    }
}
