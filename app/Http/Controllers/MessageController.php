<?php

/**
 *
 * @author     AvaWu<avawuyf@hotmail.com>
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Messages;

class MessageController extends Controller
{
    //
    public function messagesIndex()
    {
        // return Messages::all();
        return response()->json(['Success' => true,'Message' => "'replyToEmail' is an invalid email address, please check."], 200);
    }

    public function showMessages(Messages $messages)
    {
      return $messages;
    }

    public function storeMessages(Request $request)
    {
      $messages = Messages::create($request->all());

      return response()->json($messages, 201);
    }

    public function updateMessages(Request $request, Messages $messages)
    {
      $messages->update($request->all());

      return response()->json($messages, 200);
    }

    public function deleteMessages(Messages $messages)
    {
      $messages->delete();

      return response()->json(null,204);
    }

}
