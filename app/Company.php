<?php
/**
 * This is the Company model
 *
 * @author     AvaWu<avawuyf@hotmail.com>
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    //
    protected $table = 'companies';
    protected $fillable = ['companyName'];

}
