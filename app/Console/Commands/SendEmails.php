<?php
/**
 * This is the SendEmails
 *
 * @author     AvaWu<avawuyf@hotmail.com>
 */
namespace App\Console\Commands;

// use App\User;
// use App\DripEmailer;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use App\Messages;
use Carbon\Carbon;

class SendEmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    // protected $signature = 'command:name';
    // protected $signature = 'email:send {user}';
    protected $signature = 'emails:send';
    // user =avawuyf@hotmail.com

    /**
     * The console command description.
     *
     * @var string
     */
    // protected $description = 'Command description';
    protected $description = 'Send emails to a user';

    /**
     * send email service drip attribute
     *
     * @var DripEmailer
     */
    protected $drip;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        // $this->drip = $drip;
    }

    /**
     * Execute the console command.
     *in terminal
     *php artisan emails:send
     * @return mixed
     */

    public function handle()
    {
        //
        // $this->drip->send(User::find($this->argument('user')));
        //  $data = array('name' =>'testing');
        //
        //  Mail::send('welcome', $data, function ($message) {
        //
        //     $message->from('bgemelbourne@gmail.com');
        //
        //     $message->to('bgemelbourne@gmail.com')->subject('Test sending email.');
        //
        // });
        $totalMailSendNo = 0;
        try {
            $today = \Carbon\Carbon::now();
            $messages = Messages::where('status', '=', 'new')->get();
            $receivers = array();
            foreach ($messages as $message) {
                $sendAtDate = $message["sendAt"];
                if ($today >= $sendAtDate) {
                    array_push($receivers, $message["receiverEmail"]);
                }
            }
            foreach ($receivers as $receiver) {
                Mail::send([], [], function ($message) use ($receiver) {
                    $singleMess = Messages::where('receiverEmail', '=', $receiver)->where('status', '=', 'new')->first();
                    $emailSubject = $singleMess["subject"];
                    $body = $singleMess["body"];
                    $message->to($receiver)->subject($emailSubject);
                    $message->from('marketing@budgetpc.com.au');
                    $message->setBody($body, 'text/html'); // for HTML rich messages
                    $singleMess->status = 'sent';
                    $singleMess->save();
                });
                  $totalMailSendNo++;
            }

            // Mail::send([], [], function ($message) {
            //
            //   $today = \Carbon\Carbon::now();
            //   $messages = Messages::where('status', '=', 'new')->get();
            //
            //   foreach ($messages as $sendMessage)
            //   {
            //     $sendAtDate = $sendMessage["sendAtDate"];
            //       // var_dump($today >= $sendAtDate);
            //     if ($today >= $sendAtDate){
            //       $emailSubject = $sendMessage["subject"];
            //       $body = $sendMessage["body"];
            //       $recipient = $sendMessage["receiverEmail"];
            //       $message->to($recipient)->subject($emailSubject);
            //       // $message->bcc($recipient);
            //       $message->from('bgemelbourne@gmail.com');
            //       $message->setBody($body,'text/html'); // for HTML rich messages
            //       $sendMessage->status = 'sent';
            //       $sendMessage->save();
            //     }
            //
            //   }
            //   });
            $this->info($totalMailSendNo . ' emails are sent successfully!');
        } catch (\Illuminate\Database\QueryException $exception) {
            $errorInfo = $exception->errorInfo;
            return response()->json(['Message' => $errorInfo[2]], 500);
        }
    }
}
