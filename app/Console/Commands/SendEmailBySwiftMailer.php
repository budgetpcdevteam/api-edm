<?php
/**
 * This is the SendEmails
 *
 * @author     AvaWu<avawuyf@hotmail.com>
 */
namespace App\Console\Commands;
// require_once '/home/user/Desktop/newsletterAPI/api-edm/vendor/autoload.php';
require_once __DIR__.'/../../../vendor/autoload.php';
// use App\User;
use Illuminate\Mail\MailServiceProvider;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\User;
use Swift_Mailer;
use Swift_SmtpTransport;
use Swift_Message;
use Swift_Plugins_DecoratorPlugin;
use Swift_Plugins_AntiFloodPlugin;
use Config;
use Illuminate\Support\Str;
// use vendor\swiftmailer\SmtpTransport;
use App\Messages;
use Carbon\Carbon;

class SendEmailBySwiftMailer extends Command{
    /**
    * The name and signature of the console command.
    *
    * @var string
    */
    protected $signature = 'emails:sendBySwiftMailer';
    // user =avawuyf@hotmail.com

    /**
    * The console command description.
    *
    * @var string
    */
    // protected $description = 'Command description';
    protected $description = 'Send emails to a user with swiftmailer (batch)';

    /**
    * send email service drip attribute
    *
    * @var DripEmailer
    */
    protected $drip;

    /**
    * Create a new command instance.
    *
    * @return void
    */
    public function __construct()
    {
        parent::__construct();

        // $this->drip = $drip;
    }

    /**
    * Execute the console command.
    *in terminal
    *php artisan emails:sendBySwiftMailer
    * @return mixed
    */

    public function handle()
    {
        // $config = require('path/to/config.php');
        $totalMailSendNo = 0;
        // $currentMessageId = '';
        $errorMessageIds = array();
        try {
            //Create the Transport
            $emailConfig = Config::get('mail');
            // $transport = (new Swift_SmtpTransport($emailConfig['host'], $emailConfig['port']))
            // ->setUserName($emailConfig['username'])
            // ->setPassword($emailConfig['password'])
            // ;

            $numSent = $errorMessages =  array();
            $today = \Carbon\Carbon::now();
            $messages = self::_getAndMarkMessages($today);

            foreach ($messages as $message) {
                try{
                    $userId = $message['userId'];
                    $user = User::where('id', '=', $userId)->firstOrFail();
                    $host = $user['newsletter_sender_host'];
                    $port = $user['newsletter_sender_port'];
                    $encryption = trim($user['newsletter_sender_encryption']) != ''? $user['newsletter_sender_encryption'] : null;
                    $hostUserName = $user['newsletter_host_username'];
                    $hostPassword = $user['newsletter_host_password'];


                    $transport = (new Swift_SmtpTransport($host, $port, $encryption))
                    ->setUserName($hostUserName)
                    ->setPassword($hostPassword)
                    ;
                    $mailer = new Swift_Mailer($transport);
                    // And specify a time in seconds to pause for (180 secs)
                    // $mailer->registerPlugin(new Swift_Plugins_AntiFloodPlugin(200, 180));
                    $mailer->registerPlugin(new Swift_Plugins_AntiFloodPlugin(200, 180));

                    if(!array_key_exists($message['subject'], $numSent)){
                        $numSent[$message['subject']] = 0;
                    }

                    $replacements = [];
                    $recipients = json_decode($message['receiverEmail']);
                    // var_dump($recipients);
                    foreach ($recipients as $recipient){
                        $replacements[$recipient] = [
                            '{ENCRYPT_RECIPIENT_EMAIL_ADDRESS}'=>base64_encode($recipient)
                        ];
                    }
                    // print_r($replacements);
                    $decorator = new Swift_Plugins_DecoratorPlugin($replacements);
                    $mailer->registerPlugin($decorator);

                    $senderArr = explode('|', $message['senderEmail']);
                    if (count($senderArr) > 1){
                    	$sender = array($senderArr[0] => $senderArr[1]);
                    }else{
                    	$sender = $message['senderEmail'];
                    }
                    // $numSent[]
                    $swiftMessage = (new Swift_Message())
                    // Give the message a subject
                    ->setSubject($message['subject'])

                    // Set the From address with an associative array
                    // ->setFrom(['marketing@budgetpc.com.au'])
                    ->setFrom($sender)

                    // Set the To addresses with an associative array (setTo/setCc/setBcc)
                    // ->setTo(json_decode($message['receiverEmail']))

                    // Give it a body
                    ->setBody($message['body'], 'text/html')
                    ->setReturnPath($message['replyToEmail'])
                    ;
                    $message->save();
                    foreach ($recipients as $recipient) {
                        $swiftMessage-> setTo($recipient);
                        $numSent[$message['subject']] += $mailer->send($swiftMessage);
                    }
                    $message->status = 'sent';
                    $message->save();
                    $this->info('message (id = '.$message->id.', subject = '.$message->subject.') is sent successfully');
                }catch (\Exception $exception) {
                    $message->status = 'fail';
                    $message->save();
                    $this->info('message (id = '.$message->id.') sending failed, error Message: '.$exception->getMessage());
                    Log::error('message (id = '.$message->id.') sending failed, error Message: '.$exception->getMessage());
                }

            }
            foreach ($numSent as $key=>$sent){
                $this->info($sent . ' emails are sent successfully for '.$key.'!');
            }

        } catch (\Exception $exception) {
            return response()->json(['Message' => $exception->getMessage()], 500);
        }

    }
    private static function _getAndMarkMessages($time)
    {
    	$randId = Str::random(32);
        $messages = Messages::where('status', '=', 'new')->where('transId', '=', '')->where('sendAt', '<=', $time)->get();
        if(count((array) $messages) > 0){
            foreach($messages as $message){
                $message->transId = $randId;
                $message->status = 'sending';
                $message->save();
            }
        }
        return Messages::where('status', '=', 'sending')->where('transId', '=', $randId)->get();
    }

/***** testing function ************************************************************************/
/**/

  // public function handle()
  // {
  //     try {
  //       $transport = (new Swift_SmtpTransport('', 465,'ssl'))
  //                 ->setUserName('YOUR_TESTING_MAIL')
  //                 ->setPassword('YOUR_PASSWORD')
  //                 ;
  //       $users = array();
  //       $users[] = ['email' => '123@qq.com','username' => 'wrFSg', 'resetcode'=>'UUU'];
  //       $users[] = ['email' => 'r246@gmail.com','username' => 'lala', 'resetcode'=>'www'];
  //       // $replacements = [];
  //       // foreach ($users as $user) {
  //       //   $replacements[$user['email']] = [
  //       //     '{username}'=>$user['username'],
  //       //     '{resetcode}'=>$user['resetcode']
  //       //   ];
  //       // }
  //       $mailer = new Swift_Mailer($transport);
  //
  //       // $decorator = new Swift_Plugins_DecoratorPlugin($replacements);
  //       // $mailer->registerPlugin($decorator);
  //       $message = (new Swift_Message())
  //         ->setSubject('Important notice for {username}')
  //         ->setBody(
  //           "Hello {username}, you requested to reset your password.\n" .
  //           "Please visit https://example.com/pwreset and use the reset code {resetcode} to set a new password."
  //         )
  //         ->setFrom(['123321@budgetpc.com.au'])
  //         ;
  //
  //       // foreach ($users as $user) {
  //       //   $message->addTo($user['email']);
  //       // }
  //       $replacements = [];
  //       foreach ($users as $user) {
  //         $replacements[$user['email']] = [
  //           '{username}'=>$user['username'],
  //           '{resetcode}'=>$user['resetcode']
  //         ];
  //       }
  //       $decorator = new Swift_Plugins_DecoratorPlugin($replacements);
  //       $mailer->registerPlugin($decorator);
  //       foreach ($users as $user) {
  //         $message->setTo($user["email"]);
  //         $mailer->send($message);
  //       }
  //       // $mailer->send($message);
  //     } catch (\Illuminate\Database\QueryException $exception) {
  //         $errorInfo = $exception->errorInfo;
  //         return response()->json(['Message' => $errorInfo[2]], 500);
  //     }
  //
  // }

}
