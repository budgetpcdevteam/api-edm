<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnsubscribeList extends Model
{
    protected $table = 'unsubscribe_list';
    protected $fillable = ['emailAddress','companyUserId'];
}
