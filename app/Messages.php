<?php
/**
 * This is the Messages model
 *
 * @author     AvaWu<avawuyf@hotmail.com>
 */
namespace App;

use Illuminate\Database\Eloquent\Model;

class Messages extends Model
{
    //
    protected $table = 'messages';
    protected $fillable = ['subject', 'senderEmail','receiverEmail','replyToEmail','body','sendAt','status','userId', 'transId'];
    // protected $fillable = ['receiverEmail'];

    public function scopeOfId($query, $id) {
      return $query->where('id', $id);
    }
}

// {"subject":"test1", "senderEmail":"test@qq.com","receiverEmail":"test1@qq.com","replyToEmail":"test1@qq.com","body":"testbody1","sendAt":"2019-03-16","status":"sent","attachmentId":"1","userId":"2"}
