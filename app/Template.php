<?php
/**
 * This is the Template model
 *
 * @author     AvaWu<avawuyf@hotmail.com>
 */
namespace App;

use Illuminate\Database\Eloquent\Model;


class Template extends Model
{
    //
    protected $table = 'templates';
    protected $fillable = ['header','title','pageTop','pageBody','pageBottom','footer','allowNoProduct',
    'sectionBannerHtml','sectionCellHtml','RrpPriceHtml','MemberPriceHtml','SpecialPriceHtml',
    'discriptionHtml','skuHtml','singleDiscription','proNameHtml','proImageHtml','proItemTop','proItemBottom','lineProductNo','tplName','headerLink','footerLink'];
}
