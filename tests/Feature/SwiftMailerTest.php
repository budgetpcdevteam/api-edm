<?php

namespace Tests\Feature;
require_once __DIR__.'/../../vendor/autoload.php';
// require_once __DIR__.'/../../vendor/swiftmailer/swiftmailer/tests/SwiftMailerTestCase.php';
require_once __DIR__.'/../../vendor/swiftmailer/swiftmailer/tests/SwiftMailerSmokeTestCase.php';

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Mail\MailServiceProvider;
use Illuminate\Console\Command;
use Swift_Mailer;
use Swift_SmtpTransport;
use Swift_Message;
use Swift_Plugins_DecoratorPlugin;
use Swift_Plugins_AntiFloodPlugin;
use Config;
use vendor\swiftmailer\SmtpTransport;
use App\Messages;
use Carbon\Carbon;
// use swiftmailer\swiftmailer/tests/SwiftMailerSmokeTestCase;
use Artisan;
// use vendor\swiftmailer\Swift_Mailer_Test;
use App\User;
// use vendor\swiftmailer\swiftmailer\tests\SwiftMailerSmokeTestCase;
class SwiftMailerTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @test
     * @return void
     */
    public function invalideReceiverEmailWillFail()
    {
      // background
      $user = factory(User::class)->create();
      $newFakeMessge = factory(Messages::class)->create([
        'userId' => $user->id,
        'receiverEmail'=>'123@fds.com'
      ]);
      // $newFakeMessge2 = factory(Messages::class)->create([
      //   'userId' => '3'
      // ]);
      Artisan::call('emails:sendBySwiftMailer');
      $theFakeMessage = Messages::OfId($newFakeMessge->id)->first();
      $this->assertEquals('fail', $theFakeMessage->status);
      }

      /**
       * A basic test example.
       *
       * @test
       * @return void
       */
      public function emailWillSuccess()
      {

        // background
        $user = factory(User::class)->create();
        $newFakeMessge = factory(Messages::class)->create([
          'userId' => $user->id
        ]);
        $transport = $this->createMock(Swift_SmtpTransport::class);
        // $mailer = $this->createMock(Swift_Mailer::class);
        $mailTest = new SwiftMailerSmokeTestCase;
        $mailer = $mailTest->getMailer();

        $swiftMessage = $this->createMock(Swift_Message::class);
        var_dump($mailer->send($swiftMessage));
        $mailer->Transport = $transport;
        // var_dump($mailer);
        // Messages::where('status', '=', 'new')->where('id', '=', $message['id'])->get();
        $swiftMessage->subject = $newFakeMessge['subject'];
        $swiftMessage->from = ['623756672@qq.com'];
        $swiftMessage->body = $newFakeMessge['body'];
        // ->setSubject($newFakeMessge['subject'])
        // ->setFrom(['623756672@qq.com'])
        // ->setBody($newFakeMessge['body'], 'text/html');
        $number = 0;
        $recipients = json_decode($newFakeMessge['receiverEmail']);
        foreach ($recipients as $recipient) {
          // var_dump('1');
          $swiftMessage->recipient = $recipient;
          // $number += $mailer->send($swiftMessage);
          // var_dump($mailer->send($swiftMessage));
        }
        // $newFakeMessge2 = factory(Messages::class)->create([
        //   'userId' => '3'
        // ]);

        // Artisan::call('emails:sendBySwiftMailer');
        // Artisan::call('emails:sendBySwiftMailer');
        $theFakeMessage = Messages::OfId($newFakeMessge->id)->first();
        $this->assertEquals(5, $number);
        }
}
